#!/bin/sh
set -e
cd u-boot
make ten64_tfa_defconfig
make CROSS_COMPILE=${CROSS_COMPILE} -j$(nproc)
