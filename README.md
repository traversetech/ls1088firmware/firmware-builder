# Firmware builder for Traverse LS1088

This repository builds the low level firmware components for Traverse LS1088
boards (such as the Ten64 family).

Using GitLab CI these binaries are automatically uploaded to our release server.

Individual CI pipeline artifacts will also be available on GitLab for a short period of time
after the build.

The following components are built (in order of time in boot process):
* Reset Configuration Word (RCW)
* ARM Trusted Firmware
* u-boot
* DPAA2 Configuration (DPC/DPL)
* Device Tree (for EFI use)

The management complex firmware is included in the SD card and full flash images - please
see NXP's [qoriq-mc-binary](https://github.com/NXP/qoriq-mc-binary) repository for the applicable license and changelog.

Artifacts are built for both flash (QSPI) and SDcard boot modes.

For more information on updating your boards' boot firmware, see the Ten64 user manual.

## Usage
RCW and U-Boot are inputs into the Trusted Firmware build, so they have to be built before ATF:

```
./build-rcw.sh
./build-uboot.sh
./build-atf.sh
```

This produces the BL2 and BL3 binaries:
```
arm-trusted-firmware/build/ten64/debug/bl2.bin
arm-trusted-firmware/build/ten64/debug/bl2_qspi.pbl
arm-trusted-firmware/build/ten64/debug/bl2_sd.pbl
arm-trusted-firmware/build/ten64/debug/fip.bin
```

You can then build accessory files and images:

```
./build-dpaa2-config.sh
./build-sdcard.sh
```
