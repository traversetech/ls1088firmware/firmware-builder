#!/bin/sh
set -e

download_external_file() {
  url_sha256_pair="${1}"
  filename="${2}"
  extfile_url=$(echo "${url_sha256_pair}" | awk -F ' ' '{print $1}')
  extfile_sha256=$(echo "${url_sha256_pair}" | awk -F ' ' '{print $2}')
  echo "Downloading ${filename} from ${extfile_url}"
  curl "${extfile_url}" -o "${filename}"
  this_extfile_sha256=$(openssl dgst -sha256 "${filename}" | awk '{print $2}')
  if [ "${this_extfile_sha256}" != "${extfile_sha256}" ]; then
    echo "ERROR: ${filename} downloaded from ${extfile_url} does not match expected SHA256SUM"
    echo "Expected: ${extfile_sha256}"
    echo "Actual  : ${this_extfile_sha256}"
    exit 1
  fi
}

rm -rf sdcard
mkdir sdcard

PLATFORM=${PLATFORM:-ten64}
RELEASE=${RELEASE:-debug}

SDCARD_IMG="sdcard/sdcard.img"
dd if=/dev/zero of="${SDCARD_IMG}" bs=1M count=128

# IMPORTANT: If you change the partition sizes below, make sure you
# change the offsets used by dd
# Do parted -s SDCARD_IMG print to see where the partitions sit after
# alignment
#------------------
/sbin/parted -s "${SDCARD_IMG}" mklabel msdos
# Leave plenty of room for BL2 and U-Boot images that are not on the filesystem proper
/sbin/parted -s "${SDCARD_IMG}" unit MiB mkpart primary fat32 16MiB "100%"
#-------------------
# Create the firmware / kernel partiton
dd if=/dev/zero of=sdcard/boot-fat.img bs=1M count=112
/sbin/mkfs.fat sdcard/boot-fat.img
mmd -i sdcard/boot-fat.img mcfirmware
mcopy -i sdcard/boot-fat.img -s "qoriq-mc-binary/ls1088a/mc_10.20.4_ls1088a.itb" ::/mcfirmware/mc_ls1088a.itb
mcopy -i sdcard/boot-fat.img -s "qoriq-mc-binary/NXP-Binary-EULA.txt" ::/mcfirmware/

if [ -f "README.txt" ]; then
  mcopy -i sdcard/boot-fat.img -s "README.txt" ::/
fi

mmd -i sdcard/boot-fat.img dpaa2config
mcopy -i sdcard/boot-fat.img -s "dpaa2/dpc/dpc.0x1D-0x0D.dtb" ::/dpaa2config
mcopy -i sdcard/boot-fat.img -s "dpaa2/dpc/dpc.xg1_as_1g.dtb" ::/dpaa2config
mcopy -i sdcard/boot-fat.img -s "dpaa2/dpl/eth-dpl-all.dtb" ::/dpaa2config

mmd -i sdcard/boot-fat.img spi_fw
mcopy -i sdcard/boot-fat.img -s "arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/bl2_qspi.pbl" ::/spi_fw
mcopy -i sdcard/boot-fat.img -s "arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/bl2_qspi_both_sfp_1g.pbl" ::/spi_fw
mcopy -i sdcard/boot-fat.img -s "arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/bl2_qspi_xg1_1g.pbl" ::/spi_fw
mcopy -i sdcard/boot-fat.img -s "arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/fip.bin" ::/spi_fw
mcopy -i sdcard/boot-fat.img -s "dtb/fsl-ls1088a-ten64.dtb" ::/spi_fw

mkimage -T script -C none -n 'Reflash bootloader' -d flash-qspi.scr.txt sdcard/flash-qspi.scr
mcopy -i sdcard/boot-fat.img -s "sdcard/flash-qspi.scr" ::/spi_fw

if [ ! -f "recovery.itb" ]; then
  recovery_urls256=$(cat recovery-url.txt)
  download_external_file "${recovery_urls256}" "recovery.itb"
fi


if [ ! -f "openwrt-nand.ubi" ]; then
  owrtnand_urls256=$(cat openwrt-nand-url.txt)
  download_external_file "${owrtnand_urls256}" "openwrt-nand.ubi"
fi

mcopy -i sdcard/boot-fat.img -s "recovery.itb" ::/spi_fw
mcopy -i sdcard/boot-fat.img -s "openwrt-nand.ubi" ::/spi_fw

mkenvimage -s 0x80000 -o sdcard/sdcard_environment.env.bin sdcard-environment.env.txt

if [ -d add_files ]; then
  mcopy -i sdcard/boot-fat.img -s add_files ::/files
fi

dd if=sdcard/boot-fat.img of="${SDCARD_IMG}" bs=1M seek=16 conv=notrunc

dd if="arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/bl2_sd.pbl" of="${SDCARD_IMG}" bs=512 seek=8 conv=notrunc
dd if="arm-trusted-firmware/build/${PLATFORM}/${RELEASE}/fip.bin" of="${SDCARD_IMG}" bs=512 seek=2048 conv=notrunc
dd if="sdcard/sdcard_environment.env.bin" of="${SDCARD_IMG}" bs=512 seek=10240 conv=notrunc

# Sanity check
echo "Output sdcard info: "
file sdcard/sdcard.img
du -h sdcard/sdcard.img
sha256sum sdcard/sdcard.img

bzip2 -k sdcard/sdcard.img
ls -la sdcard/sdcard.img.bz2
sha256sum sdcard/sdcard.img.bz2
