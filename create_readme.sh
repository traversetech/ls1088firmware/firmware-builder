#!/bin/sh
BUILDDATE=$(date +%Y-%m-%d)

cat <<EOF > README.txt
Ten64 firmware release

Branch ${CI_COMMIT_REF_NAME} built on ${BUILDDATE}.
Commit log:
${CI_COMMIT_SHORT_SHA} ${CI_COMMIT_TITLE}
${CI_COMMIT_DESCRIPTION}

You can view the CI pipeline that built these artifacts at:
${CI_PIPELINE_URL}
You can view the repository at:
${CI_PROJECT_URL}

Usage
----------------------------------
The best way to reflash your Ten64 unit is to
use the recovery environment to run the "./flash.sh" file
inside the firmware tarball.

If your Ten64 is unable to boot from flash, you can do
a restore from microSD by writing the "rescue-sdcard.img.bz2"
(after decompressing) to a microSD card and to start your
Ten64 in SD-boot mode.

SD-boot mode can be activated by moving DIP Switch position 1
(nearest LEDs D2/D13) to the OFF position, see the quickstart
page for a reference:
https://ten64doc.traverse.com.au/quickstart/#dip-switch-and-board-led

The U-Boot menu will present a "Reflash Board" menu option.

Once the reflash is done you can move the DIP switch back to the
normal 'ON' position.

See https://ten64doc.traverse.com.au/software/firmwareupdate/
for more information on both methods.

You can also reflash individual files using U-Boot or
the recovery firmware, see the above page for details.
EOF
