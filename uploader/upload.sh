#!/bin/sh
set -e
apt-get update && apt-get -y install python3-pip rsync openssh-client jq git
pip3 install yq

rm -rf upload_output
mkdir upload_output

# Find artifacts to upload
ARTIFACTS=$(yq -r '.[]?.artifacts? | select(has("paths")) | .paths? | .[]' .gitlab-ci.yml)
for i in $ARTIFACTS; do
  cp $i upload_output
done

echo "Files to upload: "
cd upload_output
sha256sum * > SHA256SUMS
ls -la .

echo "SHA256SUMs:"
cat SHA256SUMS

cd ..


git log -1 > upload_output/BUILDINFO
echo "With submodules:" >> upload_output/BUILDINFO
git submodule >> upload_output/BUILDINFO

UPLOAD_DIST_DIR_PATH="${ARCHIVE_DIR}/${CI_COMMIT_REF_NAME}/${CI_PIPELINE_IID}"

if [ "${UPLOAD_DRIVER}" = "rsync" ]; then
  ./uploader/rsync.sh upload_output "${UPLOAD_DIST_DIR_PATH}"
else
  echo "No upload driver for ${UPLOAD_DRIVER}"
fi

echo "Upload done"
